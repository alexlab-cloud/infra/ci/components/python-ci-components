#!/bin/bash

# Extension script containing shell commands that are run once the container has been created.

# Provide permissions to the session to create and delete files
sudo chown -R vscode "$CONTAINER_WORKSPACE_FOLDER"
